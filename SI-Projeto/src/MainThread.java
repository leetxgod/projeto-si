import Utils.Read;

public class MainThread {

	public static void main(String[] args) {
		// Check load option
		boolean status = false;
		while (!status) {
			System.out.println("1 - Server\n2 - Client\n3 - Exit\n");
			int opt = Read.umInt();
			switch (opt) {
			case 1:
				status = true;
				break;
			case 2:
				status = true;
				break;
			case 3: 
				status = true;
				break;
			default:
				System.out.println("Not a valid option!");
				break;
			}
		}

	}

}

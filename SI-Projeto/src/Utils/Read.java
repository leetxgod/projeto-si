package Utils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Read {
		
		public static String umaString(){
	        String s = "";
	        try {
	            BufferedReader in = new BufferedReader (new InputStreamReader (System.in));
	            s = in.readLine();
	        }
	        catch(IOException e) {
	            System.out.println("Foi encontrado um erro!");
	        }
	        return s;
	    }

	    public static int umInt(){
	        while(true){
	        try{
	        return Integer.valueOf(umaString().trim()).intValue();
	        }
	        catch(Exception e){
	        System.out.println("N�o � um inteiro v�lido!!!");
	        }
	        }
	    }

	    public static char umChar(){
	            while(true){
	                try{
	                return umaString().charAt(0);
	                }
	        catch(Exception e){
	            System.out.println("N�o � um char v�lido!!!");
	        }
	            return (0);
	        }
	        }

	    public static boolean umBoolean(){
	        while(true){
	            try{
	                return Boolean.valueOf(umaString().trim()).booleanValue();
	            }
	        catch(Exception e){
	            System.out.println("N�o � um char v�lido!!!");
	        }
	        }
	    }
	public static double umDouble(){
	        while(true){
	            try{
	                return Double.valueOf(umaString().trim()).doubleValue();
	        }
	        catch(Exception e){
	            System.out.println("N�o � um double v�lido!!!");
	        }
	       }
	    }

	    public static long umlong(){
	        while(true){
	            try{
	                return Long.valueOf(umaString().trim()).longValue();
	        }
	        catch(Exception e){
	            System.out.println("N�o � um long v�lido!!!");
	        }
	       }
	    }

	    public static short umshort(){
	        while(true){
	         try{
	          return Short.valueOf(umaString().trim()).shortValue();
	        }
	         catch(Exception e){
	          System.out.println("N�o � um short v�lido!!!");
	        }
	       }
	    }

	    public static float umfloat(){
	        while(true){
	         try{
	          return Float.valueOf(umaString().trim()).floatValue();
	        }
	         catch(Exception e){
	          System.out.println("N�o � um float v�lido!!!");
	        }
	       }
	    }

	    public static float umbyte(){
	        while(true){
	            try{
	                return Byte.valueOf(umaString().trim()).byteValue();
	        }
	        catch(Exception e){
	            System.out.println("N�o � um byte v�lido!!!");
	        }
	       }
	    }
}

